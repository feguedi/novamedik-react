/* eslint-disable no-unused-vars */
/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
// import eslint from 'vite-plugin-eslint'

// https://vitejs.dev/config/
export default defineConfig(_ => ({
  plugins: [
    react(),
    // eslint(),
  ],
  build: {
    manifest: true,
    target: 'es2021',
    sourcemap: true,
    minify: 'esbuild',
  },
  server: {
    watch: {
      usePolling: true,
    },
    port: 3000,
  },
}));
