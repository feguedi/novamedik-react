import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@chakra-ui/react';

export function LoginBox({ children }) {
  return (
    <Box
      rounded="0.25rem"
      bg="background.100"
      color="gray.500"
      p="1rem"
      w="350px"
      maxW={{ md: '80%', sm: '90%' }}
    >
      {children}
    </Box>
  );
}

LoginBox.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]).isRequired,
};
