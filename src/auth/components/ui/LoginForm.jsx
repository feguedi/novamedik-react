import React from 'react';
import PropTypes from 'prop-types';
import { Button, Flex, Input } from '@chakra-ui/react';

export function LoginForm({
  usuario = '',
  contrasena = '',
  usuarioRef,
  contraRef,
  onInputChange,
  onSubmit,
}) {
  return (
    <Flex as="form" onSubmit={onSubmit} gap="1rem" w="100%" direction="column">
      <Input
        ref={usuarioRef}
        variant="flushed"
        px="1rem"
        color="gray.900"
        bg="white"
        w="100%"
        placeholder="Usuario"
        value={usuario}
        onChange={onInputChange}
        name="usuario"
      />
      <Input
        ref={contraRef}
        variant="flushed"
        px="1rem"
        color="gray.900"
        bg="white"
        w="100%"
        type="password"
        placeholder="Contraseña"
        value={contrasena}
        onChange={onInputChange}
        name="contrasena"
      />
      <Button colorScheme="accent" type="submit" w="100%">
        INICIAR SESIÓN
      </Button>
    </Flex>
  );
}

// https://stackoverflow.com/questions/48007326/what-is-the-correct-proptype-for-a-ref-in-react
LoginForm.propTypes = {
  usuario: PropTypes.string,
  contrasena: PropTypes.string,
  usuarioRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(HTMLInputElement) }),
  ]).isRequired,
  contraRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(HTMLInputElement) }),
  ]).isRequired,
  onInputChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

LoginForm.defaultProps = {
  usuario: '',
  contrasena: '',
};
