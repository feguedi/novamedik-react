import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@chakra-ui/react';

import hospital from '../../../assets/hospital2.jpg';

export function LoginLayout({ children }) {
  return (
    <Flex
      direction="column"
      alignItems="center"
      justifyContent="center"
      h="100vh"
      w="100%"
      bgImage={hospital}
      bgPosition="center"
      bgRepeat="no-repeat"
      bgSize="cover"
      filter=""
    >
      {children}
    </Flex>
  );
}

LoginLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]).isRequired,
};
