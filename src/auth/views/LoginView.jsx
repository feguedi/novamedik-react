/* eslint-disable object-curly-newline */
import React, { useEffect, useRef } from 'react';
import { Flex, Heading, Text } from '@chakra-ui/react';
import { LoginLayout } from '../components/layouts/LoginLayout';
import { LoginBox } from '../components/ui/LoginBox';
import { useForm } from '../../hooks/useForm';
import { LoginForm } from '../components/ui/LoginForm';

export default function LoginView() {
  const usuarioRef = useRef();
  const contraRef = useRef();
  const { usuario, contrasena, onInputChange, onResetForm } = useForm({
    usuario: '',
    contrasena: '',
  });

  const onSubmit = e => {
    e.preventDefault();
    onResetForm();
  };

  useEffect(() => {
    usuarioRef?.current.select();

    return () => {
      usuarioRef.current.value = '';
      contraRef.current.value = '';
    };
  }, [usuarioRef]);

  return (
    <LoginLayout>
      <LoginBox>
        <Flex
          alignItems="center"
          justifyContent="center"
          direction="column"
          gap="1rem"
        >
          <Heading color="primary.500" as="h2" fontWeight="300">
            iDOCTOR
          </Heading>
          <Text align="center">
            Para utilizar el sistema utiliza tus datos de usuario para iniciar
            sesión. Si tienes problemas, contacta a soporte.
          </Text>
          <LoginForm
            usuario={usuario}
            contrasena={contrasena}
            usuarioRef={usuarioRef}
            contraRef={contraRef}
            onSubmit={onSubmit}
            onInputChange={onInputChange}
          />
          <small>Diplomado Desarrollo Web 2019</small>
        </Flex>
      </LoginBox>
    </LoginLayout>
  );
}
