import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';

import AppRouter from './router/AppRouter';
import theme from './theme';

export function NovamedikApp() {
  return (
    <ChakraProvider theme={theme}>
      <AppRouter />
    </ChakraProvider>
  );
}
