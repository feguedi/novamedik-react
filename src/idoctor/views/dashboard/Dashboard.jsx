import React from 'react';
import { Divider, Flex, Heading } from '@chakra-ui/react';
import IDoctorLayout from '../../components/layouts/Layout';

export function DashboardView() {
  return (
    <IDoctorLayout>
      <Flex direction="column">
        <Heading>DashboardView</Heading>
        <Divider />
      </Flex>
    </IDoctorLayout>
  );
}
