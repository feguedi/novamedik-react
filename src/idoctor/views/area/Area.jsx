import React from 'react';
import { Divider, Flex, Heading } from '@chakra-ui/react';

export default function AreaView() {
  return (
    <Flex direction="column">
      <Heading>AreaView</Heading>
      <Divider />
    </Flex>
  );
}
