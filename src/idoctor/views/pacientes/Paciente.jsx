/* eslint-disable import/prefer-default-export */
import React from 'react';
import { Divider, Flex, Heading } from '@chakra-ui/react';

export function PacienteView() {
  return (
    <Flex direction="column">
      <Heading as="h1">PacienteView</Heading>
      <Divider />
    </Flex>
  );
}
