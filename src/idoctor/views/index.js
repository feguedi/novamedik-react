export * from './dashboard';
export * from './examenes';
export * from './habitaciones';
export * from './pacientes';
export * from './resultados';
export * from './usuarios';
