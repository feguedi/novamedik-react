import React from 'react';
import { Link } from 'react-router-dom';
import {
  Avatar,
  Flex,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from '@chakra-ui/react';
import { FaChevronDown } from 'react-icons/fa';

export default function NavbarMenu() {
  return (
    <Menu autoSelect={false} isLazy>
      <MenuButton
        aria-label="opciones"
        variant="outline"
        bg="primary.700"
        transition="all 0.2s"
        _hover={{ backgroundColor: 'primary.800', color: 'white' }}
        _active={{ backgroundColor: 'primary.800', color: 'white' }}
        rounded="0"
        ml="auto!important"
        h="100%"
      >
        <Flex
          direction="row"
          justifyContent="center"
          alignItems="center"
          px="1rem"
          gap="0.5rem"
        >
          <Avatar my="4px" name="xd" src="logo192.png" bg="background.900" />
          <FaChevronDown />
        </Flex>
      </MenuButton>
      <MenuList bg="primary.800">
        <MenuItem
          as={Link}
          to="/dashboard"
          color="white"
          _hover={{ bg: 'gray.100', color: 'black' }}
          _active={{ bg: 'gray.100', color: 'black' }}
          _focus={{ bg: 'gray.100', color: 'black' }}
        >
          Mis datos
        </MenuItem>
        <MenuItem
          color="white"
          _hover={{ bg: 'gray.100', color: 'black' }}
          _active={{ bg: 'gray.100', color: 'black' }}
          _focus={{ bg: 'gray.100', color: 'black' }}
          onClick={() => console.log('Vale por una función')}
        >
          Cerrar sesión
        </MenuItem>
      </MenuList>
    </Menu>
  );
}
