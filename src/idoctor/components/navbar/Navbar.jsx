import React from 'react';
import { Link } from 'react-router-dom';
import { chakra, Flex, Text } from '@chakra-ui/react';
import NavbarMenu from './NavbarMenu';

export default function Navbar() {
  const BrandLink = chakra(Link);
  const IDoctorNav = chakra('nav');

  return (
    <IDoctorNav
      display="flex"
      position="relative"
      bg="primary.500"
      alignItems="center"
      justifyContent="flex-start"
      minH="3.5rem"
      w="100%"
      px="0.5rem"
    >
      <Flex
        direction="row"
        justifyContent="space-between"
        justifyItems="center"
        alignItems="center"
        flexWrap="nowrap"
        px="1rem"
        mx="auto"
        my="auto"
        h="100%"
        w="100%"
        maxW={{
          sm: '300px',
          md: '720px',
          lg: '930px',
          xl: '1140px',
          '2xl': '1590px',
        }}
      >
        <BrandLink to="/">
          <Text fontSize="1.5rem" color="white" fontWeight="700">
            iDoctor
          </Text>
        </BrandLink>
        <NavbarMenu />
      </Flex>
    </IDoctorNav>
  );
}
