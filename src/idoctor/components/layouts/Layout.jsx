import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@chakra-ui/react';
import Navbar from '../navbar/Navbar';
import MenuDrawer from '../drawer/MenuDrawer';

export default function IDoctorLayout({ children }) {
  return (
    <Flex minH="100vh" bg="background.200" direction="column">
      <Navbar />
      <Flex
        direction="column"
        pt="1rem"
        w="100%"
        px="15px"
        mx="auto"
        maxW={{
          sm: '300px',
          md: '720px',
          lg: '930px',
          xl: '1140px',
          '2xl': '1590px',
        }}
      >
        {children}
      </Flex>
      <MenuDrawer />
    </Flex>
  );
}

IDoctorLayout.propTypes = {
  children: PropTypes.oneOfType([
    // PropTypes.string,
    PropTypes.element,
    PropTypes.node,
  ]),
};

IDoctorLayout.defaultProps = {
  children: <p />,
};
