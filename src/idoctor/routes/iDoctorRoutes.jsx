/* eslint-disable import/prefer-default-export */
import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { DashboardView } from '../views';

export function IDoctorRoutes() {
  return (
    <Routes>
      <Route path="dashboard" element={<DashboardView />} />
      <Route path="area" />
      <Route path="asignar" />
      <Route path="consulta" />
      <Route path="consultas" />
      <Route path="consultorios" />
      <Route path="error" />
      <Route path="examen" />
      <Route path="examenes" />
      <Route path="habitaciones" />
      <Route path="internar" />
      <Route path="paciente" />
      <Route path="pacientes" />
      <Route path="resultado" />
      <Route path="resultados" />
      <Route path="transferir" />
      <Route path="usuario" />
      <Route path="usuarios" />

      <Route path="/" element={<Navigate to="/dashboard" replace />} />
    </Routes>
  );
}
