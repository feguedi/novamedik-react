import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

import { NovamedikApp } from './NovamedikApp';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <NovamedikApp />
    </BrowserRouter>
  </React.StrictMode>
);
