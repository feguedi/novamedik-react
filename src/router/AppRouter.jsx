import React from 'react';
import { Routes, Route } from 'react-router-dom';

import LoginView from '../auth/views/LoginView';
import { IDoctorRoutes } from '../idoctor/routes/iDoctorRoutes';

export default function AppRouter() {
  return (
    <Routes>
      <Route path="login" element={<LoginView />} />
      <Route path="/*" element={<IDoctorRoutes />} />
    </Routes>
  );
}
