import { extendTheme } from '@chakra-ui/react';
import { fonts } from './foundations/fonts';
import { colors } from './foundations/colors';
import { breakpoints } from './foundations/breakpoints';

const config = {
  cssVarPrefix: 'idoctor',
  initialColorMode: 'light',
  useSystemColorMode: false,
};

const theme = extendTheme({
  breakpoints,
  colors,
  config,
  fonts,
});

export default theme;
