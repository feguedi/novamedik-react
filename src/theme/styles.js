/* eslint-disable import/prefer-default-export */
import { mode } from '@chakra-ui/theme-tools';

export const globalStyles = {
  styles: {
    global: props => ({
      body: {
        bg: mode('gray.50', 'gray.800')(props),
        // fontFamily: 'Segoe UI, Helvetica, sans-serif',
        fontFamily: '"Open Sans", sans-serif',
      },
      html: {
        // fontFamily: 'Segoe UI, Helvetica, sans-serif',
        fontFamily: 'Roboto, "Open Sans", sans-serif',
      },
      heading: {
        fontFamily: 'Roboto',
        fontWeight: 300,
      },
    }),
  },
};
