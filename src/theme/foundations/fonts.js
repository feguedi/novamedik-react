/* eslint-disable import/prefer-default-export */
import { theme as baseTheme } from '@chakra-ui/react';

export const fonts = {
  body: `"Open Sans", ${baseTheme.fonts?.body}`,
  heading: `Roboto, ${baseTheme.fonts?.heading}`,
};
