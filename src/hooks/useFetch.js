import { useEffect, useState } from 'react';

export function useFetch({
  url,
  headers = undefined,
  body = undefined,
  method = 'GET',
}) {
  const [state, setState] = useState({
    data: null,
    isLoading: false,
    hasError: null,
  });

  const options = { method };

  const getFetch = async () => {
    setState({
      ...state,
      isLoading: true,
    });

    const resp = await fetch(url, options);
    const data = await resp.json();

    setState({
      data,
      isLoading: false,
      hasError: null,
    });
  };

  useEffect(() => {
    if (headers) {
      options.headers = headers;
    }
    if (body) {
      options.body = body;
    }

    getFetch();
  }, [url]);

  return {
    data: state.data,
    isLoading: state.isLoading,
    hasError: state.hasError,
  };
}
